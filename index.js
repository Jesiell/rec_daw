const express = require('express');
var app = express();
const bodyParser = require('body-parser')
const db = require('./src/configs/sequelize')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true}))

app.use(express.static('public'))

db.sequelize.sync({alter: true}).then(() => {
  console.log("Criação do banco OK (DROP E/OU CREATE")
})

require('./src/user/routes')(app);

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/public/views/index.html")
})

app.listen(3000, () => {
  console.log("Servidor rodando na porta 3000")
})

