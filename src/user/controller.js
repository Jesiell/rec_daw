const db = require("./../configs/sequelize")
const Contato = require("./model")

exports.create = (req, res) => {
  Contato.create({
    nome : req.body.nome,
    apelido : req.body.apelido,
    email : req.body.email,
    numero : req.body.numero
  }).then((contato) =>{
    res.send(contato)
  })
}

exports.findAll = (req, res) => {
  Contato.findAll().then( contatos =>{
    res.send(contatos)
  })
}

exports.update = (req, res) => {
  Contato.update({
    nome: req.body.nome,
    apelido : req.body.apelido,
    email : req.body.email,
    numero : req.body.numero
    },
    {
      where: {
        id: req.body.id
      }
    }

  ).then(() => {
    res.send({'message' : 'ok'});
  })
}

exports.remove = (req, res) => {
  Contato.destroy({
    where: {
      id: req.body.id
    }
  }).then((affectedRows) => {
    res.send({'message' : 'ok', 'affectedRows': affectedRows})
  })
}