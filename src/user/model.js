const {Model, DataTypes} = require("sequelize")
const db = require("./../configs/sequelize")

const sequelize = db.sequelize

class Contato extends Model{}

Contato.init({
  nome: {
    type: DataTypes.STRING,
    notnull: true
  },
  apelido: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING
  },
  numero: {
    type: DataTypes.STRING
  }
  
}, {sequelize, modelName: 'contatos'})

module.exports = Contato