module.exports = (app) => {

  const controller = require('./controller')

  //criando novo contato
  app.post('/contatos', controller.create)

  //busca todos contatos
  app.get('/contatos', controller.findAll)

  //atualiza contato por ID
  app.put('/contatos', controller.update);

  //remove contato por ID
  app.delete('/contatos', controller.remove);

}