Contatos = {

  add : () => {

    var t = {};
    t.nome = $("#nome").val();
    t.apelido = $("#apelido").val();
    t.email = $("#email").val();
    t.numero = $("#numero").val();

    $.ajax({
      type: 'POST',
      url: '/contatos',
      data: t,
      dataType: 'json',
      success: Contatos.template
    })
    console.log(t);

    return false;

  },

  template : (data) => {
    console.log("entrou template");
    console.log(data.id);

    var contato = $('<tr></tr>')
      .attr('id', 'contato' + data.id)
      .attr('classs', 'contato')
      .attr('class', 'tr')

    var nome = $('<td></td>')
      .attr('class', 'nome')
      .attr('class', 'td')
      .html(data.nome);
    
    var apelido = $('<td></td>')
      .attr('class', 'apelido')
      .html(data.apelido);

    var email = $('<td></td>')
      .attr('class', 'email')
      .html(data.email);
    
    var numero = $('<td></td>')
      .attr('class', 'numero')
      .html(data.numero);

    

    $(contato).append(nome);
    $(contato).append(apelido);
    $(contato).append(email);
    $(contato).append(numero);

    $("#contato").append(contato);




  },

  findAll : () => {

    $.ajax({
      type: "GET",
      url: '/contatos',
      success: (data) => {
        for(var contato of data){
          Contatos.template(contato);
        }
      },
      error: () => {
        console.log("Ocorreu um erro no findAll!");
      },
      type: 'json'
    })
  }
}

$(document).ready(() =>{
  Contatos.findAll();
})

